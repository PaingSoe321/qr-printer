package com.tamronnetwork.tax;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.lvrenyang.io.IOCallBack;
import com.tamronnetwork.tax.mpt.myprinter.Global;
import com.tamronnetwork.tax.mpt.myprinter.WorkService;
import com.tamronnetwork.tax.mpt.utils.DataUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static android.Manifest.permission.READ_PHONE_STATE;
import static android.graphics.Color.BLACK;
import static android.graphics.Color.WHITE;

public class MainActivity extends AppCompatActivity {

    ImageView imageViewQRCode;

    EditText edtName, edtPhone, edtCode;
//    BluetoothAdapter mbluetoothAdapter;
//    BluetoothSocket mmSocket;
//    BluetoothDevice mmDevice;

    TextView myLabel;
    TextView txtName , txtPhone , txtCode;

//    OutputStream mmOutputStream;
//    InputStream mmInputStream;
//    Thread workerThread;

//    byte[] readBuffer;
//    int readBufferPosition;
//    volatile boolean stopWorker;

    Thread updateBtBtnThread, updatePrinterBtnThread;
    private String strContent;
    private Menu menu;
    private static String strName,strPhone,strCode;


    SharedPreferences settings;
    SharedPreferences.Editor editor = null;
    BluetoothAdapter mBluetoothAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageViewQRCode = (ImageView) findViewById(R.id.imageView);

        myLabel = (TextView) findViewById(R.id.label);
        txtName = (TextView) findViewById(R.id.txt_name);
        txtPhone = (TextView) findViewById(R.id.txt_phone);
        txtCode = (TextView) findViewById(R.id.txt_code);

        edtName = (EditText) findViewById(R.id.edt_name);
        edtPhone = (EditText) findViewById(R.id.edt_phone);
        edtCode = (EditText) findViewById(R.id.edt_code);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        settings = getApplicationContext().getSharedPreferences("settings", MODE_PRIVATE);


        if (WorkService.workThread == null) {

            Intent intent = new Intent(MainActivity.this, WorkService.class);
            startService(intent);
            Log.e("WorkService", "WorkServer intent stated!");

        }


        try {
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;

            if(getSupportActionBar() != null) {
//                if(ma.dev){
//                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.US); //use US time digit
//                    getSupportActionBar().setTitle("v" + ma.versionName + "    " + ma.strDb + " Development!    " +  formatter.format(new Date(BuildConfig.TIMESTAMP)));
//                }
//                else
                getSupportActionBar().setTitle("Tax  v" + versionName );
                //getSupportActionBar().setSubtitle("Version : " + versionName);
            }
            //getSupportActionBar().setTitle("Hello world App");  // provide compatibility to all the versions

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            Log.e("Tax", "ActionBar Error: " + e.getLocalizedMessage());
        }



        /* An instance of this class will be registered as a JavaScript interface */
        class MyJavaScriptInterface
        {
            public MyJavaScriptInterface(){}

            @JavascriptInterface
            @SuppressWarnings("unused")
            public void processContent(String _content)
            {
                strContent = _content;

                try {
                    JSONArray jsonArray = new JSONArray(strContent);
                    JSONObject jsonObject;

                    for (int i = 0; i < jsonArray.length(); i++) {
                        jsonObject = jsonArray.getJSONObject(i);

                        //if (jsonObject.has("id")) // inserted device id
                        //jsonObject.getInt("id");
                        try {
                            strName = jsonObject.getString("name");
                            strPhone = jsonObject.getString("phone");
                            strCode = jsonObject.getString("code");



                            Log.e("Tax", jsonObject.getString("name"));
                            Log.e("Tax", jsonObject.getString("phone"));
                            Log.e("Tax", jsonObject.getString("code"));
                        }
                        catch (Exception e){
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(MainActivity.this, getResources().getString(R.string.error) + "!", Toast.LENGTH_SHORT).show();

                                }
                            });

                        }
                    }
                }
                catch (Exception e){

                }

                Log.e("Tax", _content);
            }
        }



        //webView.getSettings().setDatabaseEnabled(true);
        //webView.setWebChromeClient(new WebChromeClient());




        if (updateBtBtnThread == null) {
            //Log.e("Tax","updateBtBtnThread == null");

            updateBtBtnThread = new Thread() {
                @Override
                public void run() {
                    while(true) {
                        //Log.e("Tax", "updateBtBtnThread == run");
                        try {
                            Thread.sleep(588);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if (mBluetoothAdapter == null) {
                            // Device does not support Bluetooth


                        } else if (!mBluetoothAdapter.isEnabled()) {
                            // Bluetooth is not enabled :)
                            //Log.e("Tax", "BT is disabled");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    //Log.e("Tax", "runOnUiThread");
                                    if (menu != null) {

                                        //Log.e("Tax", "Get MenuItem");

                                        MenuItem item = menu.findItem(R.id.action_bluetooth);
                                        if (item != null) {

                                            //Log.e("Tax", "Change MenuItem Icon");
                                            item.setIcon(getResources().getDrawable(R.mipmap.bluetoothred));
                                            //item.setEnabled(true);
                                        }
                                    }
                                }
                            });
                        } else {
                            // Bluetooth is enabled
                            //Log.e("Tax", "BT is enabled");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (menu != null) {
                                        MenuItem item = menu.findItem(R.id.action_bluetooth);
                                        if (item != null) {
                                            item.setIcon(getResources().getDrawable(R.mipmap.bluetoothblue));
                                            //item.setEnabled(true);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            };
            updateBtBtnThread.start();
        }

        /*
         * update printer bt button
         */
        // 更新按鈕狀態

        if (updatePrinterBtnThread == null) {

            updatePrinterBtnThread = new Thread() {
                @Override
                public void run() {
                    while(true) {
                        try {
                            Thread.sleep(588);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }


                        try {
                            if (WorkService.workThread.isConnecting()) {

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (menu != null) {
                                            MenuItem item = menu.findItem(R.id.action_printer);
                                            if (item != null) {
                                                item.setIcon(getResources().getDrawable(R.mipmap.printergray));
                                                //item.setEnabled(true);
                                            }
                                        }
                                    }
                                });
                            } else if (WorkService.workThread.isConnected()) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (menu != null) {
                                            MenuItem item = menu.findItem(R.id.action_printer);
                                            if (item != null) {
                                                item.setIcon(getResources().getDrawable(R.mipmap.printergreen));
                                                //item.setEnabled(true);
                                            }
                                        }
                                    }
                                });
                            } else {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (menu != null) {
                                            MenuItem item = menu.findItem(R.id.action_printer);
                                            if (item != null) {
                                                item.setIcon(getResources().getDrawable(R.mipmap.printerred));
                                                //item.setEnabled(true);
                                            }
                                        }
                                    }
                                });
                            }
                        } catch (Exception e) {

                        }
                    }
                }
            };
            updatePrinterBtnThread.start();
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_bluetooth:

                if (!mBluetoothAdapter.isEnabled()) {
                    // Bluetooth is not enabled :)
//                    startActivity(new Intent(android.provider.Settings.ACTION_BLUETOOTH_SETTINGS));
                    mBluetoothAdapter.enable();
                } else {
                    mBluetoothAdapter.disable();
                }

                //Toast.makeText(this, "Refresh action_bluetooth", Toast.LENGTH_SHORT).show();
                break;
            // action with ID action_refresh was selected
            case R.id.action_printer:

                if (!mBluetoothAdapter.isEnabled()) {// 判斷藍牙是否打開
                    mBluetoothAdapter.enable();
                    Toast.makeText(this, getResources().getString(R.string.enablingbluetooth) + ", " + getResources().getString(R.string.pleasewait), Toast.LENGTH_SHORT).show();
                    break;
                }
                if (WorkService.workThread.isConnecting()) {
                    Toast.makeText(this, getResources().getString(R.string.connectingbluetooth) + ", " + getResources().getString(R.string.pleasewait), Toast.LENGTH_SHORT).show();
                    break;
                }

                if (!WorkService.workThread.isConnected()) {// 連接藍牙

                    item.setIcon(getResources().getDrawable(R.mipmap.printergray));

                    connectPrinter();

                } else {
                    WorkService.workThread.disconnectBt();
                    item.setIcon(getResources().getDrawable(R.mipmap.printerred));

                }

                //Toast.makeText(this, "Refresh action_printer", Toast.LENGTH_SHORT).show();
                break;
            // action with ID action_refresh was selected
            case R.id.action_print:

                try {
                    Thread.sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                try{
                    if (WorkService.workThread == null)
                        break;
                    if (!WorkService.workThread.isConnected()) {
                        Toast.makeText(this, getResources().getString(R.string.connecting), Toast.LENGTH_SHORT).show();

                        connectPrinter();

                        break;
                    }
                }
                catch (Exception e){
                    Toast.makeText(this, getResources().getString(R.string.error) +": " + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                    break;
                }

                byte header[], buffer[];
                Bundle data;

                //------------- QR code -------------

                QRCodeWriter writer = new QRCodeWriter();
                try {
                    String text1 = edtName.getText().toString();
                    String text2 = edtPhone.getText().toString();
                    String text3 = edtCode.getText().toString();
                    String text = text1 + text2 + text3;
//                    if (TextUtils.isEmpty(text)) {
//                        edtName.setError("Enter the name!");
//                        edtPhone.setError("Enter the phone no!");
//                        edtCode.setError("Enter the code!");
//                    }
                    BitMatrix bitMatrix = writer.encode(text, BarcodeFormat.QR_CODE, 1024, 1024);
                    int width = bitMatrix.getWidth();
                    int height = bitMatrix.getHeight();
                    Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
                    for (int x = 0; x < width; x++) {
                        for (int y = 0; y < height; y++) {
                            bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                        }
                    }

                    data = new Bundle();
                    data.putParcelable(Global.PARCE1, bmp);
                    data.putInt(Global.INTPARA1, 256);    //384 //576 // 832
                    data.putInt(Global.INTPARA2, 0);
                    WorkService.workThread.handleCmd(Global.CMD_POS_PRINTBWPICTURE, data);

                } catch (WriterException e) {
                    e.printStackTrace();
                }

                //---------------- Text --------------------
                try {
                    header = new byte[]{0x1b, 0x40,   0x1b, 0x39, 0x01 };

                    String str =
                                    "----------------------------------------------\n" +
                                    txtName.getText().toString() + ":" + edtName.getText().toString() + "\n" +
                                    txtPhone.getText().toString() + ":" + edtPhone.getText().toString() + "\n" +
                                    txtCode.getText().toString() + ":" + edtCode.getText().toString() + "\n" +
                                    "----------------------------------------------\n" ;


                    buffer = DataUtils.byteArraysToBytes(new byte[][]{header, str.getBytes("UTF-8")});

                    data = new Bundle();
                    data.putByteArray(Global.BYTESPARA1, buffer);
                    data.putInt(Global.INTPARA1, 0);
                    data.putInt(Global.INTPARA2, buffer.length);
                    WorkService.workThread.handleCmd(Global.CMD_POS_WRITE, data);

                    //---------------- date time --------------------

                    DateFormat df = new SimpleDateFormat("MMM d, yyyy");
                    String now = df.format(Calendar.getInstance().getTime());

                    buffer = DataUtils.byteArraysToBytes(new byte[][]{header, now.getBytes("UTF-8")});

                    data = new Bundle();
                    data.putByteArray(Global.BYTESPARA1, buffer);
                    data.putInt(Global.INTPARA1, 0);
                    data.putInt(Global.INTPARA2, buffer.length);
                    WorkService.workThread.handleCmd(Global.CMD_POS_WRITE, data);


                    //---------------- print blank space --------------------
                    header = new byte[]{0x1b, 0x40,   0x1b, 0x39, 0x01 };
                    str = "";
                    for (int i = 0; i < 3; i++) str += "\n";
                    buffer = DataUtils.byteArraysToBytes(new byte[][]{header, str.getBytes("UTF-8")});
                    data = new Bundle();
                    data.putByteArray(Global.BYTESPARA1, buffer);
                    data.putInt(Global.INTPARA1, 0);
                    data.putInt(Global.INTPARA2, buffer.length);
                    WorkService.workThread.handleCmd(Global.CMD_POS_WRITE, data);

                } catch (Exception e) {
                    Log.e("Tax", "Print Error: " + e.getLocalizedMessage());
                }

                //Toast.makeText(this, "Refresh action_print", Toast.LENGTH_SHORT).show();
                break;
            // action with ID action_settings was selected



            case R.id.action_settings:


                final TextView tvPrinter = new TextView(this);
                tvPrinter.setText(getResources().getString(R.string.printer));

                String strHeader[] = {
                        "1","2","3","4","5","6","7","8","9","10"
                };
                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, strHeader);
                final Spinner spPrinter = new Spinner(this);
                spPrinter.setAdapter(arrayAdapter);
                spPrinter.setSelection(settings.getInt("printerid",1) - 1); //position

                spPrinter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        editor = settings.edit();
                        editor.putInt("printerid",  spPrinter.getSelectedItemPosition() + 1);
                        editor.apply();

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });


                LinearLayout layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.VERTICAL);

                layout.addView(tvPrinter);
                layout.addView(spPrinter);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle(getResources().getString(R.string.settings));
                alertDialog.setView(layout);

                alertDialog.setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //dialog.cancel();

                            }
                        });

                alertDialog.show();

                break;
            default:
                break;
        }

        return true;
    }


    public String getNowMyanmar(){
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", java.util.Locale.US).format(Calendar.getInstance().getTime());
    }

    public String getNow(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", java.util.Locale.US); //use US time digit
        Date curDate = new Date(System.currentTimeMillis()) ; // 獲取當前時間
        return formatter.format(curDate);
    }

    public static Bitmap resizeImage(Bitmap bitmap, int w, int h) {
        Bitmap BitmapOrg = bitmap;
        int width = BitmapOrg.getWidth();
        int height = BitmapOrg.getHeight();
        int newWidth = w;
        int newHeight = h;

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleWidth);
        Bitmap resizedBitmap = Bitmap.createBitmap(BitmapOrg, 0, 0, width,
                height, matrix, true);
        return resizedBitmap;
    }

    public static <T> T[] concat(T[] first, T[] second) {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public Bitmap combineImages(Bitmap c, Bitmap s) {

        Bitmap cs = null;

        int width, height = 0;

        width = c.getWidth();
        height = c.getHeight() + s.getHeight();

        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(cs);

        comboImage.drawBitmap(c, 0f, 0f, null);
        comboImage.drawBitmap(s, 0f, c.getHeight(), null);

        return cs;
    }





    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 88) {

            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {



            }
        }
    }
    void connectPrinter(){

        if(settings.getInt("printerid",1) == 1)
            WorkService.workThread.connectBt("66:12:CC:C4:81:F7");
        else if(settings.getInt("printerid",1) == 2)
            WorkService.workThread.connectBt("02:18:EB:77:22:C3");
        else if(settings.getInt("printerid",1) == 3)
            WorkService.workThread.connectBt("02:18:EB:77:22:C3");


    }
}
